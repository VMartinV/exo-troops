import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsapiService {
  private url ="https://newsapi.org/v2/everything?q=bitcoin&from=2019-22-19&sortBy=publishedAt&apiKey=591a4e22cbf54d8bab0896e20c0f8f5a&limit=2";
 
  constructor(private http:HttpClient) { }

  findAll(): Observable<any> {
    return this.http.get<any>(this.url);
  }
}
