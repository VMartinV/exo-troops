import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeconeComponent } from './secone.component';

describe('SeconeComponent', () => {
  let component: SeconeComponent;
  let fixture: ComponentFixture<SeconeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeconeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeconeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
