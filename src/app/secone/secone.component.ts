import { Component, OnInit } from '@angular/core';
import { NewsapiService } from '../repository/newsapi.service';

@Component({
  selector: 'app-secone',
  templateUrl: './secone.component.html',
  styleUrls: ['./secone.component.css']
})
export class SeconeComponent implements OnInit {
  articles: any;

  constructor(private repo: NewsapiService) { }

  ngOnInit() {

    this.articles = [];

    this.repo.findAll().subscribe(data => {this.articles = data.articles, console.log(this.articles.articles);
      });

    }

}
